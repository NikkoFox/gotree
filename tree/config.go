package tree

type Config struct {
	OnlyDir      bool
	AllFiles     bool
	BytesSize    bool
	HumanSize    bool
	HumanSiSize  bool
	WithoutColor bool
}
