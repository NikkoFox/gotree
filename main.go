package main

import (
	"flag"
	"gotree/tree"
	"os"
)

func main() {
	config := new(tree.Config)

	flag.BoolVar(&config.OnlyDir, "d", false, "List directories only.")
	flag.BoolVar(&config.AllFiles, "a", false, "All files are listed.")
	flag.BoolVar(&config.BytesSize, "s", false, "Print the size in bytes of each file.")
	flag.BoolVar(&config.HumanSize, "hs", false, "Print the size in a more human readable way.")
	flag.BoolVar(&config.HumanSiSize, "si", false, "Like -hs, but use in SI units (powers of 1000).")
	flag.BoolVar(&config.WithoutColor, "n", false, "Colorization off.")

	flag.Parse()
	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}
	path := flag.Arg(0)

	t := tree.Tree{
		Output: os.Stdout,
		Config: config,
		Size:   &tree.Size{Config: config, Enable: tree.IsEnabled(config)},
	}

	err := t.DirTree(path)
	if err != nil {
		panic(err.Error())
	}
}
