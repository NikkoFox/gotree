[![Go Report Card](https://goreportcard.com/badge/gitlab.com/NikkoFox/gotree)](https://goreportcard.com/report/gitlab.com/NikkoFox/gotree)
[![coverage report](https://gitlab.com/NikkoFox/gotree/badges/master/coverage.svg)](https://gitlab.com/NikkoFox/gotree/-/commits/master) [![Hits-of-Code](https://hitsofcode.com/gitlab/NikkoFox/gotree)](https://hitsofcode.com/gitlab/NikkoFox/gotree/view)


```shell
  -a    All files are listed.
  -d    List directories only.
  -hs
        Print the size in a more human readable way.
  -n    Colorization off.
  -s    Print the size in bytes of each file.
  -si
        Like -hs, but use in SI units (powers of 1000).
```
